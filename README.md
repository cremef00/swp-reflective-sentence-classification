# SWP Reflective Sentence Classification

## Data
The data folder contains the files we used to train our models, as well as preprocessed files that are generated with our code.

## Checkpoints
In the checkpoint folder, past runs from the hyperparameter search can be found. Those can be viewed using Tensorboard. Due to the size of the model files, we did not upload them (though we could provide final models in some other way). The results however should be reproducible using the code in our main.ipynb for training.

## Source
The source folder contains the code for the data preparation, training of the bag-of-words models, and fine-tuning of the transformer-based models.

## Runs
In the runs folder, the training history of our final models can be found.
